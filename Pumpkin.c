#include <studio.h>
#include <stdlib.h>
#include "MyFunctions.h"



int main(int argc,
	char *argv[]
	)}

    int numRows;
    int numCols;
    int imageSize;
    int row, col;
    int radius;
    int InOut;
    unsigned char *outImage;
    unsigned char *ptr;
    FILE *outputFP;

    printf("=====================================================\n");
    printf("I'm making a plain pixel map of a purple circle      \n");
    printf("=====================================================\n");

    if(argc != 5){
        printf("Usage : ./PurpleCircle OUTFileName numRows numCols radius \n");
        exit(1);

    }
    if( ( numRows = atoi(argv[2]) ) <= 0){  //
        printf("Error : numRows needs to be positive!\n");
    }
    if( ( numCols = atoi(argv[3]) ) <= 0){
        printf("Error : numCols needs to be positive!\n");
    }
    if( (

#include <stdio.h>
#include <stdlib.h>
#include "MyFunctions.h"
#include "HelperFunctions.c"

/*
 * This is C program that creates a purple circle as ppm. 
 * Usage: 
 *
 * PurpleCircle OutFileName numRows numCols radius
 */ 

int main(int argc,    // This is the number of things that get passed into this function.
         char *argv[] // This is the array of things passed into this function. 
        ){

    // Add here the variable declarations. 
    int numRows; 
    int numCols;
    int imageSize;
    int row, col;
    int radius;
    int InOut;           // Flag where (0 = out of the circle, 1 = in the circle)
    unsigned char *outImage;  /* Pixel Pointer  */ 
    unsigned char *ptr;       /* moving pointer */
    FILE *outputFP;           /* Out put file pointer */  

    printf("====================================================\n");
    printf("I'm making a plain pixel map of a purple cicle.     \n");
    printf("====================================================\n");

    if(argc != 5){
        printf("Usage : ./PurpleCircle OUTFileName numRows numCols radius \n");
        exit(1);
    }
    if( ( numRows = atoi(argv[2]) ) <= 0){  // <---------- Moved a ")" Here to fix code. And below too.
         printf("Error : numRows needs to be positive!\n");
    }
    if( ( numCols = atoi(argv[3]) ) <= 0){
         printf("Error : numCols needs to be positive!\n"); 
    }
    if( ( radius = atoi(argv[4]) ) <= 0){
         printf("What are you thinking! A negative value for a circles's radius? ");
    }

    printf("Values of numRows = %d , numCols = %d, radius = %d \n", numRows, numCols, radius);

    // =======================================================
    // Set up space for the pixel map. 
    // =======================================================

    imageSize = numRows*numCols*3; 
    outImage = (unsigned char *) malloc(imageSize); // space for the image. 

    if ((outputFP = fopen(argv[1], "w")) == NULL){
       perror("output opening error!");
       printf("Error could not open the output file \n");
       exit(1);
    }

    ptr = outImage; // Puts the pointer for our pixel map at its starting point in the memory. 

    for(row = 0; row < numRows; row++){
        for(col = 0; col < numCols; col++){
            // Is this pixel inside the circle? 
            InOut = InCircle(numRows, numCols, radius, row, col);

            if(InOut == 1){
                // Purple Pixel
                *ptr     = 128;
                *(ptr+1) =   0;
                *(ptr+2) = 128; 
            } else {
                // White Pixel
                *ptr     = 255;
                *(ptr+1) = 255; 
                *(ptr+2) = 255;
            }
            // Advance the pointer
            ptr += 3; 
        }
    }

    // Put it all together into a single image. 
    fprintf(outputFP, "P6 %d %d 255\n", numCols, numRows);
    fwrite(outImage, 1, imageSize, outputFP);

    /* Done */ 
    fclose(outputFP); 

    return 0; 
}

